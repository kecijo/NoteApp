package model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.TransitionRes;

import java.util.Date;

public class Notes implements Parcelable{

    private long noteId;
    private String textnote;
    private Date createdDate;
    private Users users;
    public Notes(){

    }
    public Notes (String textnote,Date createdDate){
        this.textnote = textnote;
        this.createdDate = createdDate;

    }

    protected Notes(Parcel in) {
        noteId = in.readLong();
        textnote = in.readString();
        //users = in.readParcelable(Users.class);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(noteId);
        dest.writeString(textnote);
        dest.writeParcelable(users, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Notes> CREATOR = new Creator<Notes>() {
        @Override
        public Notes createFromParcel(Parcel in) {
            return new Notes(in);
        }

        @Override
        public Notes[] newArray(int size) {
            return new Notes[size];
        }
    };

    public long getNoteId() {
        return noteId;
    }

    public void setNoteId(long noteId) {
        this.noteId = noteId;
    }

    public String getTextnote() {
        return textnote;
    }

    public void setTextnote(String textnote) {
        this.textnote = textnote;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    @Override
    public String toString() {

        return this.getTextnote();
    }
}
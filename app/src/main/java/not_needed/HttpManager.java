package not_needed;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpManager {

    public static String getData(RequestPackage p, String data){
        BufferedReader  reader = null;
        String uri = p.getUri();
        HttpURLConnection connection = null;
        try{
            URL url = new URL(uri);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod(p.getMethod());


            if (p.getMethod().equals("POST")){
                connection.setDoOutput(true);
                OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
                writer.write(data);
                writer.flush();
            }
            StringBuilder sb = new StringBuilder();
            reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            String line;
            while ((line = reader.readLine())!=null){
                sb.append(line + "\n");
            }
            return sb.toString();

        } catch (Exception e){
            e.printStackTrace();
            try {
                int status = connection.getResponseCode();
            } catch (IOException e1){
                e1.printStackTrace();
            }
            return null;
        } finally {
            if (reader!=null){
                try {
                    reader.close();
                } catch (IOException e){
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }
}

package not_needed.parsers;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import model.Users;

public class XMLParser {
    public static List<Users> parseFeed(String content) {

        try {

            boolean inDataItemTag = false;
            String currentTagName = "";
            Users users = null;
            List<Users> usersList = new ArrayList<>();

            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = factory.newPullParser();
            parser.setInput(new StringReader(content));

            int eventType = parser.getEventType();

            while (eventType != XmlPullParser.END_DOCUMENT) {

                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        currentTagName = parser.getName();
                        if (currentTagName.equals("product")) {
                            inDataItemTag = true;
                            users = new Users();
                            usersList.add(users);
                        }
                        break;

                    case XmlPullParser.END_TAG:
                        if (parser.getName().equals("product")) {
                            inDataItemTag = false;
                        }
                        currentTagName = "";
                        break;

                    case XmlPullParser.TEXT:
                        if (inDataItemTag && users != null) {
                            switch (currentTagName) {
                                case "userId":
                                    users.setUserId(Integer.parseInt(parser.getText()));
                                    break;
                                case "user":
                                    users.setUser(parser.getText());
                                    break;
                                case "password":
                                    users.setPassword(parser.getText());
                                    break;
                                default:
                                    break;
                            }
                        }
                        break;
                }

                eventType = parser.next();

            }

            return usersList;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }


    }

}

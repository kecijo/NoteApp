package not_needed.parsers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import model.Notes;
import model.Users;

public class JSONParser {
@SuppressWarnings("SimpleDateFormat")
    public static List<Notes> parseFeed(String content) {

    try {
        JSONArray ar = new JSONArray(content);

        List<Notes> notesList = new ArrayList<>();
        for (int i = 0; i < ar.length(); i++) {

            JSONObject object = ar.getJSONObject(i);
            Notes notes = new Notes();

            notes.setNoteId(object.getLong("noteId"));
            //notes.setNoteuserId(object.getLong("noteuserId"));
            notes.setTextnote(object.getString("textnote"));

            String dateStr = object.getString("createdDate");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date createdDate = sdf.parse(dateStr);
            notes.setCreatedDate(createdDate);

            notesList.add(notes);

        }
        return notesList;
    } catch (JSONException e) {
        e.printStackTrace();
        return null;

    } catch (ParseException e) {
        e.printStackTrace();
        return null;
    }
    }
    public static List<Users> parseFeedUser(String content) {

        try {
            JSONArray ar = new JSONArray(content);

            List<Users> usersList = new ArrayList<>();
            for (int i = 0; i < ar.length(); i++) {

                JSONObject object = ar.getJSONObject(i);
                Users users = new Users();

                users.setUserId(object.getLong("userId"));
                users.setUser(object.getString("user"));
                users.setPassword(object.getString("password"));

                usersList.add(users);

            }
            return usersList;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;

        }
    }
}

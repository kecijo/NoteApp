package not_needed.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import model.Users;

public class UsersDBOpenHelper{

        private static final String TABLE_NAME = "users";
        private static final String COLUMN_ID = "userId";
        private static final String COLUMN_USER = "user";
        private static final String COLUMN_PASSWORD = "password";

        public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "(" +
                COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                COLUMN_USER + " VARCHAR(20) UNIQUE, " +
                COLUMN_PASSWORD + " VARCHAR(35))";

        private static final String SELECT_FROM_TABLE = "SELECT * FROM " + TABLE_NAME + " WHERE user=?";

        private static final String SELECT_ID = "SELECT users.user_id FROM users where user_id=?";

        public static Users getUser(String username, Context context)
        {
            Users users =null;
            DBOpenHelper dbOpenHelper = new DBOpenHelper(context);
            SQLiteDatabase database = dbOpenHelper.getReadableDatabase();
            Cursor cursor = database.rawQuery(SELECT_FROM_TABLE, new String[]{username});
            cursor.moveToFirst();
            while (!cursor.isAfterLast())
            {
                users= new Users();
                users.setUserId(cursor.getLong(cursor.getColumnIndex(COLUMN_ID)));
                 users.setUser(cursor.getString(cursor.getColumnIndex(COLUMN_USER)));
                users.setPassword(cursor.getString(cursor.getColumnIndex(COLUMN_PASSWORD)));
                cursor.moveToNext();
            }
            cursor.close();
            return users;
        }
    public static long getUserID(String username, long userid, Context context)
    {
        Users users =null;
        DBOpenHelper dbOpenHelper = new DBOpenHelper(context);
        SQLiteDatabase database = dbOpenHelper.getWritableDatabase();
        Cursor cursor = database.rawQuery(SELECT_ID, new String[]{username});
        cursor.moveToFirst();
        while (!cursor.isAfterLast())
        {
            users= new Users();
            users.setUserId(cursor.getLong(cursor.getColumnIndex(COLUMN_ID)));
            users.setUser(cursor.getString(cursor.getColumnIndex(COLUMN_USER)));
            users.setPassword(cursor.getString(cursor.getColumnIndex(COLUMN_PASSWORD)));
            cursor.moveToNext();
        }
        cursor.close();
        return userid;
    }
    public static long insertUser(Users users,Context context) {
            DBOpenHelper dbOpenHelper = new DBOpenHelper(context);
            SQLiteDatabase database = dbOpenHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(UsersDBOpenHelper.COLUMN_USER, users.getUser());
            values.put(UsersDBOpenHelper.COLUMN_PASSWORD, users.getPassword());
            long insertid = database.insert(UsersDBOpenHelper.TABLE_NAME, null, values);
            users.setUserId(insertid);
            return insertid;
        }
}
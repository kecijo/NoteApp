package not_needed.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import model.Notes;

public class NotesDBOpenHelper {

    private static final String TABLE_NAME = "notes";
    private static final String COLUMN_ID = "noteId";
    private static final String COLUMN_TEXTNOTE = "textnote";
    private static final String COLUMN_DATE = "createdDate";
    private static final String COLUMN_USERID = "noteuserId";

    public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " ( " +
            COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            COLUMN_TEXTNOTE + " VARCHAR(400)," +
            COLUMN_DATE + " REAL," +
            COLUMN_USERID + " INTEGER, " +
            "FOREIGN KEY (" + COLUMN_USERID + ") REFERENCES users(userId))";

    private static final String SELECT_DATA = " SELECT * FROM notes,users "+
            " WHERE users.userId = notes.noteuserId AND users.user=? ";

    public static List<Notes> getNotesList(String username, Context context){
        List<Notes> notesList = new ArrayList<>();
        DBOpenHelper dbOpenHelper = new DBOpenHelper(context);
        SQLiteDatabase database = dbOpenHelper.getWritableDatabase();
        Cursor cursor = database.rawQuery(SELECT_DATA, new String[]{username});
        cursor.moveToFirst();
        while (!cursor.isAfterLast())
        {
            Notes notes = new Notes();
            notes.setNoteId(cursor.getLong(cursor.getColumnIndex(COLUMN_ID)));
            //notes.setNoteuserId(cursor.getLong(cursor.getColumnIndex(COLUMN_USERID)));
            notes.setTextnote(cursor.getString(cursor.getColumnIndex(COLUMN_TEXTNOTE)));
            notesList.add(notes);
            cursor.moveToNext();
        }
        cursor.close();
        return notesList;
    }

    public static Notes insertNotes(Notes notes, Context context) {
        DBOpenHelper dbOpenHelper = new DBOpenHelper(context);
        SQLiteDatabase database = dbOpenHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(NotesDBOpenHelper.COLUMN_TEXTNOTE, notes.getTextnote());
        //values.put(NotesDBOpenHelper.COLUMN_USERID, notes.getNoteuserId());
        values.put(NotesDBOpenHelper.COLUMN_DATE, notes.getCreatedDate().getTime());
        long insertid = database.insert(NotesDBOpenHelper.TABLE_NAME, null, values);
        notes.setNoteId(insertid);
        return notes;
    }

    public static Notes upgradeNotes(Notes notes, Context context) {
        DBOpenHelper dbOpenHelper = new DBOpenHelper(context);
        SQLiteDatabase database = dbOpenHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(NotesDBOpenHelper.COLUMN_TEXTNOTE, notes.getTextnote());
        values.put(NotesDBOpenHelper.COLUMN_DATE, notes.getCreatedDate().getTime());
        long id = database.update(TABLE_NAME,values,"noteId=?",new String[]{String.valueOf(notes.getNoteId())});
        notes.setNoteId(id);
        return notes;
    }

    public static Notes deleteNotes(Notes notes, Context context) {
        DBOpenHelper dbOpenHelper = new DBOpenHelper(context);
        SQLiteDatabase database = dbOpenHelper.getWritableDatabase();
        database.delete(TABLE_NAME, COLUMN_ID + "=?", new String[]{String.valueOf(notes.getNoteId())});
        return notes;
    }
}
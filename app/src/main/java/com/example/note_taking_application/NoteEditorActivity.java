package com.example.note_taking_application;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Date;

import not_needed.db.UserSharedPrefs;
import model.Notes;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.note_taking_application.MainActivity.ENDPOINT;

public class NoteEditorActivity extends Activity {

    private Notes notes;
    EditText editText;

    @Override
    protected void onCreate( Bundle savedInstanceState) throws NullPointerException {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_editor);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        editText = (EditText) findViewById(R.id.noteText);
        notes = getIntent().getParcelableExtra("id");
        if(notes !=null)
            editText.setText(notes.getTextnote());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home){

            onBackPressed();
        }
        return false;
    }

    @Override
    public void onBackPressed() {

        if(notes == null){

            Notes mNotes = new Notes();
            mNotes.setTextnote(editText.getText().toString());
            mNotes.setUsers(UserSharedPrefs.getUser(this));
            mNotes.setCreatedDate(new Date());
            requestInsertNotes("http://192.168.1.106:8080/api/notes", mNotes);
//            if(NotesDBOpenHelper.insertNotes(mNotes,this).getNoteId()!= -1){
//                startActivity(new Intent(this,NoteList.class));
//                finish();
            }else {
            notes.setTextnote(editText.getText().toString());
            notes.setUsers(UserSharedPrefs.getUser(this));
            notes.setCreatedDate(new Date());
            requestUpdateNote("http://192.168.1.106:8080/api/notes",notes);
////            if(NotesDBOpenHelper.upgradeNotes(notes,this).getNoteId()!= -1){
////                startActivity(new Intent(this,NoteList.class));
////                finish();
////            }
//        }
        }
    }

    private void requestUpdateNote(String uri, Notes notes) {

        Gson gson = new  GsonBuilder().setDateFormat( "dd-MM-yyyy hh:mm:ss").create();

        Retrofit adapter = new Retrofit.Builder()
                .baseUrl(ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        APIs api = adapter.create(APIs.class);
        api.updateNote(notes).enqueue(new Callback<Notes>() {
            @Override
            public void onResponse(Call<Notes> call, Response<Notes> response) {
                if(response.code() == 200 || response.code() == 201 || response.code() == 202){
                    startActivity(new Intent(NoteEditorActivity.this,NoteList.class));
                    finish();
                }
            }
            @Override
            public void onFailure(Call<Notes> call, Throwable t)
            {
                Toast.makeText(NoteEditorActivity.this,t.getMessage(),Toast.LENGTH_LONG).show();
            }
        });
    }

    private void requestInsertNotes(String uri, final Notes notes) {
//
//        @SuppressWarnings("SimpleDateFormat")
//     class DateDeserializer implements JsonDeserializer<Date> {
//            public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) {
//                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
//                Date date = null;
//                try {
//                    date = sdf.parse(json.getAsJsonPrimitive().getAsString());
//                    return date;
//                } catch (ParseException e) {
//                    e.printStackTrace();
//                }
//                return date;
//            }
//        }
       Gson gson = new  GsonBuilder().setDateFormat( "dd-MM-yyyy hh:mm:ss").create();

         Retrofit adapter = new Retrofit.Builder()
                .baseUrl(ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        APIs api = adapter.create(APIs.class);
        api.insertNote(notes).enqueue(new Callback<Notes>() {
            @Override
            public void onResponse(Call<Notes> call, Response<Notes> response) {
                if(response.code() == 200 || response.code() == 201){
                    Intent intent = new Intent(NoteEditorActivity.this,NoteList.class);
                    startActivity(intent);
                    finish();
                }
            }

            @Override
            public void onFailure(Call<Notes> call, Throwable t) {
                Toast.makeText(NoteEditorActivity.this,t.getMessage(),Toast.LENGTH_LONG).show();
            }
        });
    }
}
package com.example.note_taking_application;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import not_needed.db.UserSharedPrefs;
import model.Notes;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.note_taking_application.MainActivity.ENDPOINT;

public class NoteList extends ListActivity{

    public static final int EDITOR_REQUEST_CODE = 1001;
    private static final int MENU_DELETE_ID = 10001;
    private int currentNoteId;
    ArrayAdapter<Notes> noteAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        registerForContextMenu(getListView());//holds a bit and then released it will generate a menu

        requestNotesbyUser("http://192.168.1.106:8080/api/notes", UserSharedPrefs.getUser(this).getUser());
        FloatingActionButton actionButton = (FloatingActionButton) findViewById(R.id.fab);
        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NoteList.this,NoteEditorActivity.class);
                startActivity(intent);
                finish();
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    private void requestNotesbyUser(String uri, String username) {

        Gson gson = new  GsonBuilder().setDateFormat( "dd-MM-yyyy hh:mm:ss").create();
        Retrofit adapter = new Retrofit.Builder()
                .baseUrl(ENDPOINT)
                //.addConverterFactory(GsonConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        APIs api = adapter.create(APIs.class);
        api.getNotesbyUser(username).enqueue(new Callback<List<Notes>>() {
            @Override
            public void onResponse(Call<List<Notes>> call, retrofit2.Response<List<Notes>> response) {
                if (response.code() == 200) {
                    refreshDisplay(response.body());
                }
                else Toast.makeText(NoteList.this,"Couldn't load Notes",Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<List<Notes>> call, Throwable t)
            {
                Toast.makeText(NoteList.this, t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

    }

    private void requestDeleteNote(String uri, final Notes notes) {

        Gson gson = new  GsonBuilder().setDateFormat( "dd-MM-yyyy hh:mm:ss").create();
        final Retrofit adapter = new Retrofit.Builder()
                .baseUrl(ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        APIs api = adapter.create(APIs.class);

        api.deleteNote(notes).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.code() == 200){
                    //to just delete the current note not to refresh all the list of note again.
                    noteAdapter.remove(notes);
                    noteAdapter.notifyDataSetChanged();
                }
                else{
                    Toast.makeText(NoteList.this,"Something went wrong!", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(NoteList.this, t.getMessage(),Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_sign_out) {

            UserSharedPrefs.clear(this);
            Intent intent = new Intent(this,MainActivity.class);
            startActivity(intent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        currentNoteId = (int) info.id;
        menu.add(0, MENU_DELETE_ID, 0, "Delete");
    }

    private void refreshDisplay(List<Notes> notesInList) {
        noteAdapter = new ArrayAdapter<>(this, R.layout.list_item_layouut, notesInList);
        setListAdapter(noteAdapter);

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getItemId() == MENU_DELETE_ID) {

            Notes notes = noteAdapter.getItem(currentNoteId);
            requestDeleteNote("http://192.168.1.106:8080/api/notes",notes);

        }
        return super.onContextItemSelected(item);
    }


    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {

        Notes note = noteAdapter.getItem(position);
        Intent intent = new Intent(this, NoteEditorActivity.class);
        intent.putExtra("id", note);
        startActivity(intent);
//        startActivityForResult(intent, EDITOR_REQUEST_CODE);
        finish();
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode == EDITOR_REQUEST_CODE && resultCode == RESULT_OK) {
//            Notes note = new Notes();
//            NotesDBOpenHelper.insertNotes(note,this);
////            refreshDisplay();
//        }
//    }
}
package com.example.note_taking_application;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import java.util.HashMap;
import java.util.Map;

import not_needed.db.UserSharedPrefs;
import model.Users;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends Activity{

    public Button btn,btn2;
    public EditText enterUsername,enterPassword;
    public static final String ENDPOINT = "http://192.168.1.106:8080/api/";

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        if(UserSharedPrefs.getUser(this) == null) {

            setContentView(R.layout.signin_signout);
            btn = (Button) findViewById(R.id.button);
            enterUsername = (EditText) findViewById(R.id.sign_in_username);
            enterPassword = (EditText) findViewById(R.id.sign_in_password);

            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    if (isOnline())
                    {
                        Map<String, String> usersMap = new HashMap<>();
                        usersMap.put("user",enterUsername.getText().toString());
                        usersMap.put("password", enterPassword.getText().toString());
                        requestData("http://192.168.1.106:8080/api", usersMap);

                    }else {
                        Toast.makeText(MainActivity.this,"Network isn't available",Toast.LENGTH_LONG).show();
                    }
                }
            });

            btn2 = (Button) findViewById(R.id.button2);
            btn2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(MainActivity.this, Credentials.class);
                    startActivity(intent);
                    finish();
                }
            });
        }
        else {
            Intent intent = new Intent(MainActivity.this, NoteList.class);
            startActivity(intent);
            finish();
        }
    }

    private void requestData(String uri, Map<String, String > userMap) {

        Retrofit adapter = new Retrofit.Builder()
                .baseUrl(ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        APIs api = adapter.create(APIs.class);

        api.verifyUser(userMap).enqueue(new Callback<Users>() {
            @Override
            public void onResponse(Call<Users> call, retrofit2.Response<Users> response) {

                if (response.code() == 200) {
                    if (response.body().getUser().equals(enterUsername.getText().toString()) && response.body().getPassword().equals(enterPassword.getText().toString())) {

                        UserSharedPrefs.saveUser(MainActivity.this, response.body());
                        Intent intent = new Intent(MainActivity.this, NoteList.class);
                        startActivity(intent);
                        finish();
                    } else {
                        Toast.makeText(MainActivity.this, "Your Username and Password do not match", Toast.LENGTH_LONG).show();
                    }
                }
                else {
                    Toast.makeText(MainActivity.this,"User doesn't exist",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Users> call, Throwable t) {
                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
            }


        });

    }

    protected boolean isOnline() {
        ConnectivityManager cm  = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo!=null && netInfo.isConnectedOrConnecting())
        {
            return true;
        }
        else return false;
    }
}
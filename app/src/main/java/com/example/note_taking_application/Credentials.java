package com.example.note_taking_application;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.text.TextWatcher;

import not_needed.db.UserSharedPrefs;
import model.Users;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.note_taking_application.MainActivity.ENDPOINT;

public class Credentials extends Activity {

    public EditText signInName,signInPassword,signInConfirmPassword;

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credentials);
        signInName = (EditText) findViewById(R.id.sign_up_username);
        signInName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                requestGetUsers("http://192.168.1.106:8080/api/user",signInName.getText().toString());
            }
        });
        signInPassword = (EditText) findViewById(R.id.sign_up_password);
        signInConfirmPassword = (EditText) findViewById(R.id.confirm);

        Button button = (Button) findViewById(R.id.OkButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                    requestGetUsers("http://192.168.1.106:8080/api/user",signInName.getText().toString());
                    if (signInPassword.getText().toString().equals(signInConfirmPassword.getText().toString())) {

                        Users newUser = new Users();
                        newUser.setUser(signInName.getText().toString());
                        newUser.setPassword(signInPassword.getText().toString());
                        requestAddUser("http://192.168.1.106:8080/api/user",newUser);
//                        if(UsersDBOpenHelper.insertUser(users, Credentials.this) != -1)
//                        {
//                            Intent intent = new Intent(Credentials.this, NoteList.class);
//                            startActivity(intent);
//                            finish();
//                        }
//
//                    } else {
//                        Toast.makeText(Credentials.this,"Passwords do not  match!",Toast.LENGTH_LONG).show();
//                    }
//                }else {
//                    Toast.makeText(Credentials.this,"Username already exists!",Toast.LENGTH_LONG).show();
//                }
            }
        }
        });
    }

    private void requestAddUser(String uri, Users newUser) {
        Retrofit adapter = new Retrofit.Builder()
                .baseUrl(ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        APIs api = adapter.create(APIs.class);
        api.addUser(newUser).enqueue(new Callback<Users>() {
            @Override
            public void onResponse(Call<Users> call, Response<Users> response) {
                if (response.code() == 200 || response.code() == 201) {
                    UserSharedPrefs.saveUser(Credentials.this, response.body());
                    Intent intent = new Intent(Credentials.this, NoteList.class);
                    startActivity(intent);
                    finish();
                }
            }

            @Override
            public void onFailure(Call<Users> call, Throwable t) {
                Toast.makeText(Credentials.this,"Passwords do not  match!",Toast.LENGTH_LONG).show();
            }
        });

    }

    private void requestGetUsers(String uri, String username) {
        Retrofit adapter = new Retrofit.Builder()
                .baseUrl(ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        APIs api = adapter.create(APIs.class);
        api.getUsers(username).enqueue(new Callback<Users>() {
            @Override
            public void onResponse(Call<Users> call, Response<Users> response) {
                if (response.body() == null ) {
                    Toast.makeText(Credentials.this, "Available username", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(Credentials.this, "Username already exists!", Toast.LENGTH_SHORT).show();
                }

            }
            @Override
            public void onFailure(Call<Users> call, Throwable t) {
                Toast.makeText(Credentials.this,t.getMessage(),Toast.LENGTH_LONG).show();


            }

        });
    }

    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}

package com.example.note_taking_application;

import android.provider.ContactsContract;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;

import model.Notes;
import model.Users;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface APIs {

    @POST("login/")
    Call<Users> verifyUser(@Body  Map<String, String> userMap);

    @GET("notes?")
    Call<List<Notes>> getNotesbyUser (@Query("username") String username);

    @HTTP(method = "DELETE", path = "notes", hasBody = true)
    Call<ResponseBody> deleteNote(@Body Notes notes);

    @POST("notes/")
    Call<Notes> insertNote(@Body Notes notes);

    @POST("user/")
    Call<Users> addUser(@Body Users users);

    @GET("user/")
    Call<Users> getUsers(@Query("user") String username);

    @PUT("notes/")
    Call<Notes> updateNote(@Body Notes notes);




}
